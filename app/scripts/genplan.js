
// ready
'use strict';

$(document).ready(function () {

  let videoIn = $('.b-video');
  let genplanSub = $('.genplan-main__sub');
  let genplanV = $('.genplan-main__video');
  let genplanItem = $('.genplan-main__item');
  let screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

  function dynamicVideo() {
    videoIn.load();
    videoIn.on('loadstart', function (event) {
      $('body').addClass('loading');
    });
    videoIn.on('canplay', function (event) {
      $('body').removeClass('loading');
    });
    videoIn[0].play();
  }

  function hoverPopup(elem, sub, unique) {
    $(elem).mouseover(function (event) {
      let mess;
      let dataA = $(this).data('avail');
      let dataF = $(this).data('floor');
      let dataLink = $(this).data('link');
      let dataNum = $(this).data('num');

      let SVGwidth = $(window).width();
      let SVGheight = $(document).outerHeight();
      let position = $(this).position();
      let left = position.left;
      let top = position.top;
      let right = SVGwidth - left;
      let bottom = SVGheight - event.pageY;
      let rightPer = right * 100 / SVGwidth;

      if (typeof dataA !== typeof undefined && dataA !== false) {
        if (dataA != '0') {
          if (dataLink) {
            if (dataNum) {
              mess = "<div class='svg-popover__left'><b class='text--grey text--uppercase'>" + dataNum + "<br> свободен</b></div><div class='svg-popover__right'><a href='" + dataLink + "' class='text--white text--uppercase'><b>Подробнее...</b></a></div>";
            } else {
              mess = "<div class='svg-popover__left'><b class='text--grey text--uppercase'>" + dataF + "<br> этаж</b></div><div class='svg-popover__right'><a href='" + dataLink + "' class='text--white text--uppercase'>" + dataA + " офисов свободно</a></div>";
            }
          } else {
            mess = "<div class='svg-popover__left'><b class='text--grey text--uppercase'>" + dataF + "<br> этаж</b></div><div class='svg-popover__right'>" + dataA + " офисов свободно</div>";
          }
        } else {
          if (dataLink) {
            if (dataNum) {
              mess = "<div class='svg-popover__left'><b class='text--grey text--uppercase'>" + dataNum + "<br> занят</b></div><div class='svg-popover__right svg-popover__right--un'><a href='" + dataLink + "' class='text--white text--uppercase'><b>Подробнее...</b></a></div>";
            } else {
              mess = "<div class='svg-popover__left'><b class='text--grey text--uppercase'>" + dataF + "<br> этаж</b></div><div class='svg-popover__right svg-popover__right--un'><div>Нет свободных помещений<br><a href='" + dataLink + "' class='text--primary text--uppercase'>Подробнее...</a></div></div>";
            }
          } else {
            mess = "<div class='svg-popover__left'><b class='text--grey text--uppercase'>" + dataF + "<br> этаж</b></div><div class='svg-popover__right svg-popover__right--un'><div>Нет свободных помещений<br><a href='" + dataLink + "' class='text--primary text--uppercase'>Подробнее...</a></div></div>";
          }
        }
        if(sub == true) {
          $('.genplan-pop').addClass('active').css({
            'right': '30%', 'bottom': bottom + 'px'
          }).find('.genplan-pop__wrapper').html(mess);
        } else {
          $('.genplan-pop').addClass('active').css({
            'right': 'calc(' + rightPer + '% - 50px)', 'bottom': bottom + 20 + 'px'
          }).find('.genplan-pop__wrapper').html(mess);
        }
      }
    }).mouseout(function () {
      if(unique) {
        $('.genplan-pop__wrapper').html('');
      }
    });
  }
  hoverPopup('polygon', false, true);
  hoverPopup('path', false, true);

  function dynamicPlanIn(classId, videoIdMIn, imgId, videoIdMOut, svgId) {
    $('.' + classId).click(function () {

      $('.b-video .b-video__mp4').attr("src", videoIdMIn);
      $('.genplan-pop__formobile').removeClass('active');

      let dataB = $(this).parent().data('build');
      let dataSQ = $(this).parent().data('square');
      let dataOff = $(this).parent().data('offices');
      let dataSQs = $(this).parent().data('squares');
      let dataFl = $(this).parent().data('floor');
      let dataFr = $(this).parent().data('free');
      let dataBs = $(this).parent().data('busy');
      let dataIn = $(this).parent().data('info');
      let dataSvg = $(this).parent().data('svg');
      genplanSub.show().addClass(classId);
      genplanV.addClass('animated fadeIn');

      let svgImg = '<image x="0" y="0" width="100%" height="900px" preserveaspectratio="none" xlink:href="' + imgId + '"></image>' + svgId;

      dynamicVideo();

      videoIn.on("ended", function () {
        $('.genplan-build--js').html(dataB);
        $('.genplan-square--js').html(dataSQ);
        $('.genplan-offices--js').html(dataOff);
        $('.genplan-squares--js').html(dataSQs);
        $('.genplan-floor--js').html(dataFl);
        $('.genplan-free--js').html(dataFr);
        $('.genplan-busy--js').html(dataBs);
        $('.genplan-info--js').html(dataIn);
        $('.genplan-main__item svg').html(svgImg);
        genplanV.addClass('fadeOut');
        genplanItem.addClass('animated fadeIn');

        hoverPopup('polygon', true);

        setTimeout(function () {
          genplanV.removeClass('animated fadeIn fadeOut');
        }, 1000);
      });

      $('.' + classId + ' .genplan-back--js').click(function () {
        $('.b-video .b-video__mp4').attr("src", videoIdMOut);
        $('.genplan-pop').removeClass('active').find('.genplan-pop__wrapper').html('');

        $('.svg-popover').removeClass('active').find('.svg-popover__hide').html('');
        genplanItem.addClass('fadeOut');
        genplanV.addClass('animated fadeIn');

        dynamicVideo();

        videoIn.on("ended", function () {
          genplanV.addClass('fadeOut');
          genplanItem.removeClass('animated fadeIn fadeOut');
          setTimeout(function () {
            genplanV.removeClass('animated fadeIn fadeOut');
            $('.' + classId + ':not(.svg-popover__hide)').hide();
            genplanSub.removeClass(classId);
          }, 1000);
        });
        return false;
      });

      return false;
    });
  }

  $('.svg-main rect').each(function () {
    let paths = $(this);
    //data attributes
    let dataB = $(this).data('build');
    let dataT = $(this).data('type');
    let dataA = $(this).data('avail');
    let dataC = $(this).data('class');
    let dataR = $(this).data('reverse');
    let dataSQ = $(this).data('square');
    let dataOff = $(this).data('offices');
    let dataSQs = $(this).data('squares');
    let dataFl = $(this).data('floor');
    let dataFr = $(this).data('free');
    let dataBs = $(this).data('busy');
    let dataIn = $(this).data('info');
    let dataVin = $(this).data('vin');
    let dataVout = $(this).data('vout');
    let dataImg = $(this).data('vimg');
    let dataSvg = $(this).data('svg');

    //positions
    let SVGheight, top;
    if (screen_width <= 767) {
      SVGheight = $(document).outerHeight();
      top = $(this).position().top;
    } else {
      SVGheight = $('.svg-main').height();
      top = $(this).attr('y');
    }
    let SVGwidth = $('.svg-main').width();
    let left = $(this).position().left;
    let right = SVGwidth - left;
    let bottom = SVGheight - top;
    let topPer = top * 100 / SVGheight;
    let bottomPer = bottom * 100 / SVGheight;
    let rightPer = right * 100 / SVGwidth;
    let leftPer = left * 100 / SVGwidth;

    let mess = "<div class='svg-popover__inner'>Лит <b>" + dataB + "</b></div><div class='svg-popover__hide'></div><div class='genplan-pop__formobile'></div>";
    let mydiv = $('<div/>', {
      'class': 'svg-popover',
      html: mess,
      'data-type': dataT,
      'data-avail': dataA,
      'data-class': dataC,
      'data-build': dataB,
      'data-square': dataSQ,
      'data-offices': dataOff,
      'data-squares': dataSQs,
      'data-floor': dataFl,
      'data-free': dataFr,
      'data-busy': dataBs,
      'data-info': dataIn,
      'data-vin': dataVin,
      'data-vout': dataVout,
      'data-vimg': dataImg,
      'data-svg': dataSvg
    });
    if (dataR) {
      if (screen_width <= 767) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 5 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 767 && screen_width <= 900) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 7 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 900 && screen_width <= 1000) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 7 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 1000 && screen_width <= 1100) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 15 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 1100 && screen_width <= 1200) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 7 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 1200 && screen_width <= 1300) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 7 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 1300 && screen_width <= 1400) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 7 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 1400 && screen_width <= 1500) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 4 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 1500 && screen_width <= 1600) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 3 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 2100 && screen_width <= 2300) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer - 4 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 2300 && screen_width <= 2500) {
        console.log(screen_width);
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer - 8 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 2500 && screen_width <= 2700) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer - 10 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 2700 && screen_width <= 3000) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer - 12 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 3000) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer - 14 + '%' }).addClass('svg-popover--reverse');
      } else {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer - 4 + '%' }).addClass('svg-popover--reverse');
      }
    } else {
      if (screen_width <= 767) {
        mydiv.css({ 'right': rightPer - 4 + '%', 'bottom': bottomPer + 2 + '%' });
      } else if (screen_width > 767 && screen_width <= 900) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 9 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 900 && screen_width <= 1000) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 9 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 1000 && screen_width <= 1100) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 12 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 1100 && screen_width <= 1200) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 12 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 1200 && screen_width <= 1300) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 9 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 1300 && screen_width <= 1400) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 6.5 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 1400 && screen_width <= 1500) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 4 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 1500 && screen_width <= 1600) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer + 3 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 2100 && screen_width <= 2300) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer - 6 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 2300 && screen_width <= 2500) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer - 10 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 2500 && screen_width <= 2700) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer - 12 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 2700 && screen_width <= 3000) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer - 14 + '%' }).addClass('svg-popover--reverse');
      } else if (screen_width > 3000) {
        mydiv.css({ 'left': leftPer + 1 + '%', 'bottom': bottomPer - 18 + '%' }).addClass('svg-popover--reverse');
      } else {
        mydiv.css({ 'right': rightPer - 4 + '%', 'bottom': bottomPer - 4 + '%' });
      }
    }
    if (dataB) {
      $('.popovers--js').append(mydiv);
    }
  });

  $('.svg-popover__inner').mouseover(function () {
    $('.genplan-pop__formobile').removeClass('active');
    $('.svg-popover__inner').removeClass('active');
    $('.svg-popover').removeClass('active').find('.svg-popover__hide').html('');
    $(this).addClass('active');
    let dataB = $(this).parent().data('build');
    let dataT = $(this).parent().data('type');
    let dataA = $(this).parent().data('avail');
    let dataC = $(this).parent().data('class');
    let dataFl = $(this).parent().data('floor');
    let dataVin = $(this).parent().data('vin');
    let dataVout = $(this).parent().data('vout');
    let dataImg = $(this).parent().data('vimg');
    let dataSvg = $(this).parent().data('svg');
    let mess;
    if (dataA != '0') {
      mess = "<div class='svg-popover__left'><b class='text--grey'>Литер " + dataB + "</b><br>" + dataT + "</div><div class='svg-popover__right'>" + dataA + " офисов свободно</div>";
      if (screen_width <= 767) {
        $(this).parent().addClass('active').find('.genplan-pop__formobile').addClass('active ' + dataC).html(mess);
      } else {
        $(this).parent().addClass('active').find('.svg-popover__hide').addClass(dataC).html(mess);
      }
    } else {
      mess = "<div class='svg-popover__left'><b class='text--grey'>Литер " + dataB + "</b><br>" + dataT + "</div><div class='svg-popover__right svg-popover__right--un'><div>Нет свободных помещений <br><span class='text--primary text--uppercase'>Подробнее...</span></div></div>";
      if (screen_width <= 767) {
        $(this).parent().addClass('active').find('.genplan-pop__formobile').addClass('active --un ' + dataC).html(mess);
      } else {
        $(this).parent().addClass('active').find('.svg-popover__hide').addClass('--un ' + dataC).html(mess);
      }
    }
    dynamicPlanIn(dataC, dataVin, dataImg, dataVout, dataSvg);
  }).mouseout(function () {
    // $('.genplan-pop').removeClass('active').html('');
  });
  $('.svg-main').mouseover(function () {
    $('.genplan-pop__formobile').removeClass('active');
    $('.svg-popover__inner').removeClass('active');
    $('.svg-popover').removeClass('active').find('.svg-popover__hide').html('');
  });
  $('.sub-svg image').mouseover(function () {
      $('.genplan-pop').removeClass('active').find('.genplan-pop__wrapper').html('');
  });


});
//# sourceMappingURL=genplan.js.map
